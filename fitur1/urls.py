from django.conf.urls import url
from .views import request_token, index, request_auth, auth_logout
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^authenticate/', request_auth, name='authenticate'),
    url(r'^request-token/', request_token , name='get_token'),
    url(r'^logout/', auth_logout , name='logout'),
]
