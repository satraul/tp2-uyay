function hideReplies(id){
	var list = document.getElementById("reply-"+id);
	var button = document.getElementById("btn-show-comment-"+id);
	list.setAttribute("hidden", "true");
	button.href = "javascript:showReplies(" + id +",true)";
	button.innerHTML = "Show replies";
}

function showReplies(id, isCalled){
	var comment_section = document.getElementById("reply-"+id);
	var list = document.getElementById("reply-list-"+id);
	var button = document.getElementById("btn-show-comment-"+id);

	if (!isCalled) {
		console.log("initiating ajax call");

		$.ajax({
			type:"GET",
			url:"/fitur4/get_comment/?id=" + id,
			success: function(result) {
				console.log("ajax call success");
				var reply_list = $("#reply-list-"+id);
				console.log(result.length);
				console.log(result);
				result.forEach(function(item, index, arr){
					var name = item['name'];
					var date = item['created_on']
					var content = item['content'];
					text_content = '<li class="list-group-item">' +
						'<h5 class="card-title">' + name + '</h5>' + 
						'<h6 class="card-subtitle text-muted">' + date + '</h6>' + 
						'<p class="card-text">' + content + '</p>' + 
						'</li>';
					reply_list.append(text_content);
				});
				button.href = "javascript:hideReplies(" + id + ")";
				button.innerHTML = "Hide replies"
				comment_section.removeAttribute("hidden");

			},
			error: function(result) {
				alert("error cuy");
			}
		});
	} else {
		button.href = "javascript:hideReplies(" + id + ")";
		button.innerHTML = "Hide replies";
		comment_section.removeAttribute("hidden");
	}
}


function sendReply(id){
	var replyData = {
		id: id, 
		name: $('#reply-'+id+' input[name=name]').val(),
		content: $('#reply-'+id+' textarea[name=content]').val(),
		csrfmiddlewaretoken: $('#reply-'+id+' input[name=csrfmiddlewaretoken]').val(),
	}
	var reply_list = $("#reply-list-"+id);

	console.log(replyData)

	$.ajax({
			type: "POST",
			url: "/fitur4/add_comment/",
			data: JSON.stringify(replyData),
			dataType: 'json',
			contentType: 'application/json',
			// endcode: true,
			success: function(result){
				var data = result['data'];
				var name = data['name'];
				var date = data['created_on'];
				var content = data['content'];
				text_content = '<li class="list-group-item">' +
					'<h5 class="card-title">' + name + '</h5>' + 
					'<h6 class="card-subtitle text-muted">' + date + '</h6>' + 
					'<p class="card-text">' + content + '</p>' + 
					'</li>';
				reply_list.append(text_content);
			},
			error: function(result) {
				alert(result['error']);
				console.log(result)
			}
	});
}
