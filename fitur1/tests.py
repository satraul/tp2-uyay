from django.test import TestCase
from django.test import Client

# Create your tests here.
class Fitur1UnitTest(TestCase):
    def test_fitur1_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_fitur1_is_exist_with_session(self):
        session = self.client.session
        session['session-id'] = '123'
        session.save()

        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)