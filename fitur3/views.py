from django.shortcuts import render, redirect
from fitur1.models import Company
from .models import Forum, Comment
from .forms import ForumForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
response = {}
def index(request):
    if 'session-id' not in request.session or 'company_id' not in request.session:
        response['flag'] = False;
        return redirect('/')
    company = Company.objects.filter(companyID=request.session["company_id"]).first()
    forum_list = Forum.objects.all().filter(company=company)
    paginator = Paginator(forum_list, 3)

    page = request.GET.get('page')
    try:
        forum = paginator.page(page)
    except:
        forum = paginator.page(1)

    response['forum'] = forum
    response['flag'] = True;

    return render(request, 'forum.html', response)

def add(request):
    if 'session-id' not in request.session or 'company_id' not in request.session:
        return redirect('/')
    if request.method == 'POST':
        form = ForumForm(request.POST)
        response['form'] = form
        if form.is_valid():
            company = Company.objects.filter(companyID=request.session["company_id"]).first()
            Forum.objects.create(content=form.cleaned_data['content'], company=company)
            forum = Forum.objects.all()
            response['forum'] = forum
            return redirect('/forum/')
        else:
            forum = Forum.objects.all()
            response['forum'] = forum
            response['form'] = form
            return render(request, 'test.html', response)
    return redirect('/forum/')

def delete_forum(request, id):
    if 'session-id' not in request.session:
        return redirect('/')
    to_delete = Forum.objects.get(id=id)
    to_delete.delete()
    return redirect('/forum/')
