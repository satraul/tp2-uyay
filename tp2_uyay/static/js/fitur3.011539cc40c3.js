function checkLoggedIn() {
  if (IN.User.isAuthorized() === true) {
      $('section').css("display", "inline")
      $("#space-logout").append('<li class="nav-item"><a class="btn btn-danger" onclick="logout()">Logout</a></li>');
  } else {
      window.location = "/";
  }
}
function onError(error) {
  console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout(removeProfileData);
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
    window.location = "/";
}
